<?php
require_once "vendor/autoload.php";

use app\controllers\ControllerAccueil;
use app\controllers\ControllerProduit;
use app\controllers\ControllerPanier;
use app\controllers\ControllerUtilisateur;
use app\controllers\ControllerErreur;
use app\controllers\ControllerCommande;
use app\controllers\ControllerLivraison;

use Illuminate\Database\Capsule\Manager as DB;

session_start(); //début de la variable SESSION

if(!isset($_SESSION['panier'])) {
    $_SESSION['panier'] = array();
}

$db = new DB();
$db->addConnection((parse_ini_file("/src/conf/db.ini"))); //connexion a la base a l'aide des informations de db.ini
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/', function () { //affiche accueil
    $v = new ControllerAccueil();
    $v->accueil();
})->name('accueil');

$app->get('/utilisateur', function () { //liste des utilisateurs
    $v = new ControllerUtilisateur();
    $v->listeUtilisateur();
})->name('listeUtilisateur');


$app->get('/utilisateur/:id', function ($id) { //détail d'un utilisateur
    $c = new ControllerUtilisateur();
    $c->afficheUtilisateur($id);
})->name('utilisateur');

$app->get('/produits', function () { //liste des produits
    $v = new ControllerProduit();
    $v->listeProduit();
})->name('listeProduit');

$app->post('/produits', function () { //recherche d'un produit
    $c = new ControllerProduit();
    $c->rechercheProduit();
})->name('listeProduitPost');


$app->get('/produit/:id', function ($id) { //détail d'un produit
    $c = new ControllerProduit();
    $c->afficheProduit($id);
})->name('produit');

$app->get('/ajoutProduit', function () { //formulaire ajout d'un produit
    $c = new ControllerUtilisateur();
    $c->verifieAccesFournisseur();

    $c = new ControllerProduit();
    $c->vueAjoutProduit();
})->name('ajoutProduit');

$app->post('/ajoutProduit', function () { //enregistrement d'un produit
    $c = new ControllerUtilisateur();
    $c->verifieAccesFournisseur();

    $c = new ControllerProduit();
    $c->ajoutProduit();
})->name('ajoutProduitPost');

$app->get('/panier', function () { //affichage panier
    $c = new ControllerPanier();
    $c->affichePanier();
})->name('panier');

$app->post('/ajoutPanier', function () { //ajax ajout dans panier
    $c = new ControllerPanier();
    $c->ajoutePanier();
})->name('ajoutPanierPost');

$app->post('/modifiePanier', function () { //ajax modifie quantité panier
    $c = new ControllerPanier();
    $c->modifiePanier();
})->name('modifiePanierPost');

$app->post('/supprimePanier', function () { //ajax supprime du panier
    $c = new ControllerPanier();
    $c->supprimePanier();
})->name('supprimePanierPost');

$app->get('/videPanier', function () { //vide le panier
    $c = new ControllerPanier();
    $c->videPanier();

    $app = \Slim\Slim::getInstance();
    $app->redirect('panier'); //redirige vers l'url de panier
})->name('videPanier');

$app->get('/totalPanier', function () { //récupère valeur total du panier
    $c = new ControllerPanier();
    $c->totalPanier();
})->name('totalPanier');

$app->get('/inscription', function () { //formulaire d'inscription
    $c = new ControllerUtilisateur();
    $c->vueAjoutUtilisateur();
})->name('inscription');

$app->post('/inscription', function () { //enregistrement inscription
    $c = new ControllerUtilisateur();
    $c->ajoutUtilisateur();
})->name('inscriptionPost');

$app->get('/connexion', function () { //formulaire connexion
    $c = new ControllerUtilisateur();
    $c->vueConnexion();
})->name('connexion');

$app->post('/connexion', function () { //vérification connexion
    $c = new ControllerUtilisateur();
    $c->connexionUtilisateur();
})->name('connexionPost');

$app->get('/deconnexion', function () { //confirmation deconnexion
    $c = new ControllerUtilisateur();
    $c->vueDeconnexion();
})->name('deconnexion');

$app->post('/deconnexion', function () { //deconnecte
    $c = new ControllerUtilisateur();
    $c->deconnexionUtilisateur();
})->name('deconnexionPost');

$app->get('/accesRefuse', function () { //page accès refusé
    $c = new ControllerErreur();
    $c->afficheAccesRefuse();
})->name('accesRefuse');

$app->get('/commande', function () { //verifie connexion pour la commande
    $c = new ControllerCommande();
    $c->verfieCommande();
})->name('commande');

$app->get('/passageCommande', function () { //enregistre commande
    $c = new ControllerCommande();
    $c->passageCommande();
})->name('passageCommande');

$app->get('/livraison', function () { //liste livraison d'un fournisseur
    $c = new ControllerUtilisateur();
    $c->verifieAccesFournisseur();

    $c = new ControllerLivraison();
    $c->afficheLivraison();
})->name('livraison');

$app->post('/livraison', function () { //affiche type de recherche livraison
    $c = new ControllerUtilisateur();
    $c->verifieAccesFournisseur();

    $c = new ControllerLivraison();
    $c->rechercheLivraison();
})->name('livraisonPost');

$app->post('/livraisonEff', function () { //change etat livraison
    $c = new ControllerUtilisateur();
    $c->verifieAccesFournisseur();

    $c = new ControllerLivraison();
    $c->livraisonEff();
})->name('livraisonEffPost');

$app->run();