jQuery(document).ready(function(){
    /**
     * appel ajax ajout du produit dans le panier
     */
    eventAjoutPanier = function () {
        $('.formPanier').submit(function (e) {
            e.preventDefault();
            var parent = $(this).parent();
            var form = $(this);
            $.ajax({
                url: '/web/ajoutPanier',
                type: 'post',
                data: {
                    'id': $(this).closest(".divProduit").attr('idProd'),
                    'qty': $(this).find('.qty').val()
                },
                success: function () {
                    $(form).remove();
                    $(parent).append('<p><span class="bg-success">Produit ajouter au panier</span></p>')
                }
            });
        });
    };

    /**
     * appel ajax pour la recherche des produits
     */
    $('#rechercheProduit,#categorie,#rechercheProducteur').on('input', function () {
        $.ajax({
            url:'/web/produits',
            type:'post',
            data: {
                'nom': $('#rechercheProduit').val(),
                'categ': $('#categorie').val(),
				'producteur': $('#rechercheProducteur').val()
            },
            success:function(data){
                $('#produits').html(data);
                eventAjoutPanier();
            },
            error: function (e) {
                console.log(e.responseText);
            }
        });
    });

    eventAjoutPanier();
});