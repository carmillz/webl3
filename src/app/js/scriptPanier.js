/**
 * script concernant le panier
 */
jQuery(document).ready(function(){
    /**
     * event sur la modification de la quantité avec actualisation des données
     */
    function eventPanier() {
        $('.modifierPanier').off('click');
        $('.supprimerPanier').off('click');

        $('.modifierPanier').on('click', function () { //modification de la quantité du panier
            if($('.modifierPanier').parent().find('.formModifPanier').length==0) {
                //affiche l'input
                $(this).parent().append('<form class="formModifPanier">' +
                    '<input required type=\'number\' min=\'1\' class=\'qty\'>' +
                    '<input class=\'btn btn-primary modifierPanier\' value=\'Ok\' type=\'submit\'>' +
                    '</form>');
                $('.modifierPanier').focus();

                divProd = $(this).closest('.divProduit');
                //appale ajax modification de la quantité
                $('.formModifPanier').submit(function (e) {
                    e.preventDefault();
                    $.ajax({
                        url: '/web/modifiePanier',
                        type: 'post',
                        data: {
                            'id': $(this).closest(".divProduit").attr('idProd'),
                            'qty': $(this).find('.qty').val()
                        },
                        success: function (data) {
                            $(divProd).replaceWith(data);

                            $.ajax({ //actualise le total du panier
                                url: '/web/totalPanier',
                                type: 'get',
                                success: function (data) {
                                    $('#totalPanier').text(data);
                                }
                            });

                            $('.formModifPanier').remove();
                            eventPanier();
                        }
                    });
                });
            }else {
                $('.modifierPanier').parent().find('.formModifPanier').remove();
            }
        });

        /**
         * appel ajax suppression d'un produit du panier
         */
        $('.supprimerPanier').on('click', function () {
            divProd = $(this).closest('.divProduit');
            idProd = $(divProd).attr('idProd');
            $.ajax({
                url:'/web/supprimePanier',
                type:'post',
                data: {
                    'id': idProd
                },
                success:function(){
                    $(divProd).hide();
                }
            });
        });
    }

    eventPanier();
});
