/**
 * script pour page livraison
 */
jQuery(document).ready(function() {

    /**
     * au changement des options
     */
    $('#livreCheck').change(function() {
        var eff = false;
        if($(this).is(":checked")) {
            eff = true;
        }
        $.ajax({
            url:'/web/livraison',
            type:'post',
            data: {
                'effectue': eff
            },
            success:function(data){
                $('#livraisons').html(data);
                eventEffectue();
            }
        });
    });

    /**
     * event ajax passage d'une livraison en effectuer
     */
    eventEffectue = function () {
        $('.formLivraison').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            $.ajax({
                url:'/web/livraisonEff',
                type:'post',
                data: {
                    'idLivraison': $(this).closest(".divLivraison").attr('idLivr')
                },
                success:function(data){
                    $(form).closest('.divLivraison').replaceWith(data);
                },error: function (e) {
                }
            })
        })
    }

    eventEffectue();
});