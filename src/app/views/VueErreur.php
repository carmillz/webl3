<?php

namespace app\views;

class VueErreur
{

    /**
     * méthode render affiche le type de page en fonction de la méthode
     * 0->page indique accès refusé
     * @param $methode
     */
    function render($methode) {
        include "header.php";
        switch ($methode) {
            case 0 :
                ?>
                <div>
                    <h1>Vous n'avez pas accès a cette page</h1>
                    <p>
                        <?php
                        $app = \Slim\Slim::getInstance();
                        $urlAccueil = $app->urlFor('accueil');
                        $urlConnection = $app->urlFor('connexion');
                        print "<a class=\"btn btn-primary\" href=\"$urlAccueil\">Accueil</a>
                            <a class=\"btn btn-primary\" href=\"$urlConnection\">Se connecter</a>";
                        ?>
                    </p>

                </div>
                <?php
                break;
        }
        include "footer.php";
    }
}