<?php

namespace app\views;

use app\models\Utilisateur;
use Slim\Slim;
use app\models\Produit;
use app\models\Producteur;
class VueProduit
{
    private $app;

    /**
     * méthode render affiche le type de page en fonction de la méthode
     * 0->affiche la liste de produit
     * 1->détail d'un produit
     * 2->formulaire d'ajout d'un produit
     * 3->indique produit bien enregistré
     * @param $methode
     * @param null $produit
     */
    function render($methode, $produit=null) {
		include "header.php";
        switch ($methode) {
            case 0 :
                $script = dirname($_SERVER['SCRIPT_NAME'])."/src/app/js/scriptProduit.js";
                echo "<script type=\"text/javascript\" src=\"$script\"></script>"; //ajout du script jquery pour le panier

                $this->app = Slim::getInstance();
                $urlListeProduit = $this->app->urlFor('listeProduit');
                ?>
				<div class="col-md-3" id="options">
					<h1> Recherche... </h1>
					<h3> Par nom </h3>
					<input type='text' name="rechercheProduit" placeholder="Search" id="rechercheProduit" > <br>
					<h3> Par catégorie </h3>
					<select name="Catégories" id="categorie">
                        <option value="tous" selected> Tous </option>
						<option value="alimentaire"> Alimentaire </option>
						<option value="service"> Services </option>
						<option value="materiel"> Matériel </option>
						<option value="custom"> Customisé </option>
					</select> <br>
					<h3> Par producteur </h3>
					<input type='text' name="rechercheProducteur" placeholder="Search" id="rechercheProducteur" >
				</div>
				<div class="col-md-9" id="produits">
					<h1> Produits </h1>
					<?php
                    $produits = Produit::all();
                    $this->affichageProduits($produits);
					?>
				</div>
                <script type="application/javascript">
                    $('.divProduit > a').each(function () {
                        var button = this;
                        var id = $(button).parent().attr('idProd');
                        $(button).attr('href', "produit/"+id);
                    })
                </script>
                <?php
				break;

			case 1 : ?>
				<div class="col-md-3" id="options">
                    <?php

					print "<h1> $produit->nomProduit </h1>
                    <img class='imgProduit' src=\"/web/image/produit/$produit->image\">";
                    ?>
				</div>
				<div class="col-md-9" id="produits">
					<h1> Détails </h1>
					<?php 
						$utilisateur = Utilisateur::where('idUtilisateur', '=', $produit->idProducteur)->first();
						$producteur = $utilisateur->producteur;
						print "
							<h4>Prix: </h4>
							<p>$produit->prix €/$produit->unité </p>
							<h4>Fournisseur: </h4>
							<p>$producteur->nomProducteur</p>
							<h4> Description :</h4>
							<p> $produit->description </p>
							<h4>Détails de livraison: </h4>
							<p> $producteur->detailsLivraison</p>
							<div>
								<h4>Quantité : </h4><input type=\"number\" step=\"any\" />
								<a class=\"btn btn-primary\" href=\"\">Ajouter au panier</a>
							</div>	
						";
					
					?>
				</div>
			<?php
                break;
            case 2 :
                ?>
                <div class="col-md-3"></div>
                <div class="col-md-9">
                    <h1>Ajout d'un produit</h1>
                    <form href="ajoutProduit" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="nomProduit">Nom du produit</label>
                            <input class="form-control" type="text" name="nomProduit">
                        </div>
                        <div class="form-group">
                            <label for="catagorie">Categorie</label>
                            <select class="form-control" name="categorie">
                                <option value="alimentaire">Alimentaire</option>
                                <option value="service">Services</option>
                                <option value="materiel">Matériel</option>
                                <option value="custom">Customisé</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="prix">Prix</label>
                            <input class="form-control" type="number" step="0.01" min="0" name="prix">
                        </div>
                        <div class="form-group">
                            <label for="prix">Unité</label>
                            <input class="form-control" type="text" name="unite">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <br>
                            <textarea class="form-control" name="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Image du produit</label>
                            <input class="form-control" type="file" name="image" />
                        </div>
                        <input class="btn btn-primary" type="submit">
                    </form>
                </div>
                <?php
                break;
            case 3 :
                ?>
                <div>
                    <h2>Ajout du produit réussi</h2>
                </div>
<?php
        }
		include "footer.php";
    }

    /**
     * div d'un produit dans la liste des produits
     * @param $p
     */
    function divProduit($p) {
        $urlProduit = $this->app->urlFor('produit', array('id'=>$p->idProduit));
		$util = $p->producteur;
		$prod = $util->producteur;
        print "<div class='divProduit col-md-12' idProd='$p->idProduit' prod='$p->nomProduit' categ='$p->categorie'>
            <div class='col-md-4'>
                <img class='imgProduit' src=\"image/produit/$p->image\">
            </div>
            <div class='col-md-8'>
                <h2> $p->nomProduit </h2>
                <p>prix: $p->prix €/$p->unité</p>
				<p>Fournisseur: $prod->nomProducteur</p>
                <p> $p->description </p>
                <a class='btn btn-info' href=\"$urlProduit\">Détails</a>
                <form class='formPanier'>
                    <input required type='number' min='1' class='qty'>
                    <input class='btn btn-primary ajoutPanier' value='Ajout panier' type='submit'>
                </form>
            </div>
        </div>";
    }

    /**
     * boucle affichage des produits
     * @param $produits
     */
    function affichageProduits($produits) {
        $this->app = \Slim\Slim::getInstance();
        foreach ($produits as $p){
            $this->divProduit($p);
        }
    }
}