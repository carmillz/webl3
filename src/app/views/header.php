<?php
$app = \Slim\Slim::getInstance();

$urlAccueil = $app->urlFor('accueil');
$urlProduits = $app->urlFor('listeProduit');
$urlAjoutProduit = $app->urlFor('ajoutProduit');
$urlInscription = $app->urlFor('inscription');
$urlConnexion = $app->urlFor('connexion');
$urlPanier = $app->urlFor('panier');
$urlDeconnexion = $app->urlFor('deconnexion');
$urlLivraison = $app->urlFor('livraison');
$urlUtilisateur = $app->urlFor('listeUtilisateur');

$bootstrap = dirname($_SERVER['SCRIPT_NAME'])."/src/app/views/css/bootstrap.min.css";
$site = dirname($_SERVER['SCRIPT_NAME'])."/src/app/views/css/site.css";

$bootstrapJS = dirname($_SERVER['SCRIPT_NAME'])."/src/app/js/bootstrap.bundle.js";
$jquery = dirname($_SERVER['SCRIPT_NAME'])."/src/app/js/jquery.min.js";


echo "<!DOCTYPE html>
<html>

<head>
	<meta charset=\"utf-8\" />
	<title>Application Inteva</title>
	
	<link rel=\"stylesheet\" href=\"$bootstrap\" />
	<link rel=\"stylesheet\" href=\"$site\" />
</head>



<body>

<script type=\"text/javascript\" src=\"$jquery\"></script>
<script type=\"text/javascript\" src=\"$bootstrapJS\"></script>

<nav class=\"navbar navbar-default navbar-fixed-top\">
        <div class=\"container-fluid\">
          <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
              <span class=\"sr-only\">Toggle navigation</span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"$urlAccueil\">e-commerce</a>
          </div>
          <div id=\"navbar\" class=\"navbar-collapse collapse\">
            <ul class=\"nav navbar-nav\">
              <li class=\"active\"><a href=\"$urlAccueil\">Accueil</a></li>
              <li><a href=\"$urlProduits\">Produits</a></li>
              <li><a href='$urlUtilisateur'>Nos fournisseurs</a></li>
			  
              ";
if (!isset($_SESSION['pseudo'])) {
	/*
	* S'il n'y a pas de pseudo : personne n'est connecté : on ne peut pas ajouter de produit ni se deconnecter
	*/
	echo "	  
            </ul>
            <ul class=\"nav navbar-nav navbar-right\">
              <li class=\"active\"><a href=\"$urlPanier\">Panier<span class=\"sr-only\">(current)</span></a></li>
              <li><a href=\"$urlConnexion\"/navbar-static-top/\">Connexion</a></li>
			  <li><a href=\"$urlInscription\"/navbar-fixed-top/\">Inscription</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

<div class=\"container\">";
} else {
	/*
	* S'il y a un pseudo : quelqu'un est connecté : on ne peut pas se connecter
	*/
	if (isset($_SESSION['fournisseur'])){
		if ($_SESSION['fournisseur']==1){
			/*
			* Si la personne connectée est un founisseur, elle peut ajouter des produits
			*/
			echo "<li><a href=\"$urlAjoutProduit\">Ajout produit</a></li>
	              <li><a href='$urlLivraison'>Mes livraisons</a></li>    
            ";
		}
	}
/*
* Si la personne soit ou non un fournisseur
*/
$pseudo = $_SESSION['pseudo'];
$c = new \app\controllers\ControllerUtilisateur();
$urlCompte = $app->urlFor('utilisateur', array('id'=>$c->getIdUtilisateur()));
echo "	  
				</ul>
				<ul class=\"nav navbar-nav navbar-right\">
				  <li><a>Bonjour <b>$pseudo</b></a></li>
				  <li class=\"active\"><a href=\"$urlPanier\">Panier<span class=\"sr-only\">(current)</span></a></li>
				  <li><a href=\"$urlDeconnexion\">Deconnexion</a></li>
				  <li><a href=\"$urlCompte\">Mon Compte</a></li>
				</ul>
			  </div><!--/.nav-collapse -->
			</div><!--/.container-fluid -->
		</nav>

	<div class=\"container\">";	
}

?>