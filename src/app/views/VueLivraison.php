<?php

namespace app\views;

use app\controllers\ControllerUtilisateur;
use app\models\Livraison;

class VueLivraison
{
    public $app;

    /**
     * méthode render affiche le type de page en fonction de la méthode
     * 0->liste par défault des livraisons du fournisseur
     * @param $methode
     */
    function render($methode) {
        include "header.php";

        $this->app = \Slim\Slim::getInstance();
        switch ($methode) {
            case 0 :
                $script = dirname($_SERVER['SCRIPT_NAME'])."/src/app/js/scriptLivraison.js";
                echo "<script type=\"text/javascript\" src=\"$script\"></script>"; //ajout du script jquery pour les livraisons
                ?>
                <div>
                    <div class="col-md-9">
                        <h1>Mes livraisons</h1>
                        <div id="livraisons">
                            <?php
                            $c = new ControllerUtilisateur();
                            $idUtil = $c->getIdUtilisateur();
                            $livraisons = Livraison::where('idLivreur', '=', $idUtil)->where('effectue', '=', 0)->get();
                            $this->affichageLivraisons($livraisons);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h1>Options</h1>
                        <div>
                            <label for="dejaLivre">Afficher les livraisons effectuées</label>
                            <input id="livreCheck" type="checkbox" name="dejaLivre">
                        </div>
                    </div>
                </div>
                <?php
                break;
        }
        include "footer.php";
    }

    /**
     * affiche le div d'un produit
     * @param $l la livraison
     */
    function divLivraison($l) {
        $urlProduit = $this->app->urlFor('produit', array('id'=>$l->idProduit));
        $p = $l->produit; //le produit
        $c = $l->client; //le client
        $time = strtotime($l->dateCommande);
        $date = date("d/m/Y", $time); //date du jour
        $prixTotal = $p->prix*$l->quantité; //prix total de la livraison
        $urlCompte = $this->app->urlFor('utilisateur', array('id'=>$c->idUtilisateur));

        print "<div class='divLivraison col-md-12' idLivr='$l->idLivraison'>
            <div class='col-md-6'>
                <h2> $p->nomProduit </h2>
                <p>quantité : <b>$l->quantité $p->unité</b>, prix total : <b>$prixTotal</b> €</p>
                <p> $p->description </p>
                <a class='btn btn-info' href=\"$urlProduit\">Détails</a>
            </div>
            <div class='col-md-6'>
                <h2>Info client</h2>
                <p>client : <a href='$urlCompte'>$c->pseudo</a></p>
                <p>date de la commande : $date</p>";

        if($l->effectue == 0) { //si la livraison n'est pas affectue
            print "<form class='formLivraison'>
                    <input class='btn btn-primary livrEff' value = 'Passer \"Livraison effectuée\"' type = 'submit' >
                </form >";
        }else { //si la livraison est effectue
            print "<span>Livraison déjà effectuée</span>";
        }
        print "</div>
        </div>";
    }

    /**
     * boucle pour l'affichage des livraisons
     * @param $livraisons
     */
    function affichageLivraisons($livraisons) {
        $this->app = \Slim\Slim::getInstance(); //initialise app
        if(!$livraisons->isEmpty()) {
            foreach ($livraisons as $l) {
                $this->divLivraison($l);
            }
        }else {
            print "<h3>Vous n'avez pas de livraison en cours pour le moment</h3>";
        }

    }
}