<?php

namespace app\views;
use Slim\Slim;
use app\models\Utilisateur;
use app\models\Producteur;

class VueUtilisateur
{
    /**
     * méthode render affiche le type de page en fonction de la méthode
     * 0->formulaire inscription
     * 1->confirmation inscrit
     * 2->formulaire connexion
     * 3->confirmation déconnection
     * 4->liste des fournisseurs
     * 5->détail d'un utilisateur
     * @param $method
     * @param null $utilisateur
     */
	function render($method, $utilisateur=null){
		include "header.php";
		switch ($method){
			case 0:
				?>
                <div class="col-md-offset-3 col-md-9">
					<h1> Enregistrement d'un utilisateur </h1>
						<form class="col-md-9" href="ajoutUtilisateur" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="pseudo">Pseudo</label>
                                <input class="form-control" type="text" name="pseudo" maxlength="11"><br>
							</div>
							<div class="form-group">
								<label for="nom">Nom</label>
								<input class="form-control" type="text" name="nom" maxlength="11"><br>
							</div>
							<div class="form-group">
								<label for="prenom">Prenom</label>
								<input class="form-control" type="text" name="prenom" maxlength="11"><br>
							</div>
							<div class="form-group">
								<label for="mdp">Mot de Passe</label>
								<input class="form-control" type="password" name="mdp"><br>
							</div>
							<div class="form-group">
								<label for="type">Êtes-vous un fournisseur ou un client ?</label>
								<br>
								<input type="radio" name="type" value="fournisseur" checked> Fournisseur
								<br>
								<input type="radio" name="type" value="client"> Client
							</div>
							<div class="form-group">
								<label for="nomEnt">Nom de l'entreprise</label>
								<input class="form-control" type="text" name="nomEnt" maxlength="20"><br>
							</div>
							<div class="form-group">
								<label for="adresse">Adresse de l'entreprise</label>
								<input class="form-control" type="text" name="adresse" maxlength="40"><br>
							</div>
							<div class="form-group">
								<label for="detailLiv">Détails de livraison</label>
								<input class="form-control" type="text" name="detailLiv" maxlength="250"><br>
							</div>
							<input class="btn btn-primary" type="submit">
						</form>
				</div>
			<?php
			break;
			case 1: ?>
				<div class="col-md-9">
					<h1> Vous êtes bien inscrit </h1>
						<?php
                            $app = \Slim\Slim::getInstance();
                            $urlConnexion = $app->urlFor('connexion');
                            print "<p> Vous pouvez maintenant vous connecter <a href=\"$urlConnexion\">ici</a>"
                        ?>
				</div>
			<?php
			
			break;
			case 2: ?>
				<div class="col-md-9">
					<h1> Se connecter </h1>
					<form class="col-md-9" href="connexionUtilisateur" method="post" enctype="multipart/form-data">
							<div class="form-group">
								<label for="pseudo">Pseudo</label>
                                <input class="form-control" type="text" name="pseudo" maxlength="11"><br>
							</div>
							<div class="form-group">
								<label for="mdp">Mot de Passe</label>
								<input class="form-control" type="password" name="mdp"><br>
							</div>
							<input class="btn btn-primary" type="submit">
                            <?php
                            $app = \Slim\Slim::getInstance();
                            $urlInscription = $app->urlFor('inscription');
                            print "<a href=\"$urlInscription\">Pas de compte ?</a>"
                            ?>
					</form>
				</div>
			<?php
			
			break;
			case 3: ?>
				<div class="col-md-9">
					<h1> Voulez-vous vraiment vous déconnecter ?</h1>
					<form href="connexionUtilisateur" method="post" enctype="multipart/form-data">
						<input class="btn btn-primary" type="submit">
					</form>
				</div>
				
			<?php
			break;
			case 4: 
				$this->app = Slim::getInstance();
				?>
				<div class="col-md-9" id="utilisateurs">
					<h1> Utilisateurs </h1>
					<?php
                    $utilisateurs = Utilisateur::where('fournisseur', '=', 1)->get();
                    $this->affichageUtilisateur($utilisateurs);
					?>
				</div>
			<?php
			break;
			case 5: 
				?>
				<div class="col-md-3" id="utilisateur">
					<h1> <?php echo $utilisateur->pseudo ?> </h1>
				</div>
				<div class="col-md-9" id="produits">
					<h1> Détails </h1>
					<?php 
						$utilisateur = Utilisateur::where('idUtilisateur', '=', $utilisateur->idUtilisateur)->first();
						print "
							<h4>Nom: </h4>
							<p>$utilisateur->nom </p>
							<h4>Prénom: </h4>
							<p>$utilisateur->prenom</p>
							";
						if ($utilisateur->fournisseur){
							$producteur = Producteur::where('idUtilisateur', '=', $utilisateur->idUtilisateur)->first();
							print "
								<h4>Nom de l'entreprise</h4>
									<p> $producteur->nomProducteur </p>
								<h4>Adresse: </h4>
									<p> $producteur->adresseProducteur </p>
								<h4>Détails de livraison: </h4>
									<p> $producteur->detailsLivraison </p>
							";
						}
						
					
					?>
				</div>
				
			<?php
			break;
		}
		include "footer.php";
	}

    /**
     * div d'un utilisateur dans la liste des fournisseurs
     * @param $u
     */
	function divUtilisateur($u) {
        $urlUtilisateur = $this->app->urlFor('utilisateur', array('id'=>$u->idUtilisateur));
        $producteur = $u->producteur;
		print "<div class='divProduit col-md-12'>
            <div class='col-md-8'>
                <h2> $u->pseudo </h2>
                <p>$u->nom $u->prenom</p>
                <p>$producteur->nomProducteur</p>
                <p>$producteur->adresseProducteur</p>
                <p><a href=\"$urlUtilisateur\">Détails...</a></p>
            </div>
        </div>";
    }

    /**
     * boucle affichage des fournisseurs
     * @param $utilisateur
     */
    function affichageUtilisateur($utilisateur) {
        $this->app = \Slim\Slim::getInstance();
        foreach ($utilisateur as $u){
            $this->divUtilisateur($u);
        }
    }
}
?>