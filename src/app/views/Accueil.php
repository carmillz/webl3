<?php

namespace app\views;

use Slim\Slim;

class Accueil
{

    /**
     * méthode render affiche le type de page en fonction de la méthode
     * 0->page d'accueil
     * @param $methode
     */
    function render($methode) {
        include "header.php";
        switch ($methode) {
            case 0 :
                $app = Slim::getInstance();
                $urlListeProduit = $app->urlFor('listeProduit');
                ?>
				<h1> Bonjour et bienvenue !</h1>
				<h3> Sur ce site, vous pourrez :</h3>
				<ul>
					<li> vendre toutes sortes de marchandises en tant qu'artisan,
							maraîchers, éleveurs... N'importe quel entrepreneur 
							peut ici proposer ses services</li>
					<li> profiter des resources proposées par ces artisans </li>
				</ul>
                <a class="btn btn-primary" href=<?php print $urlListeProduit ?>>Produits</a>
                <?php
        }

        include "footer.php";
    }
}