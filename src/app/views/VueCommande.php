<?php

namespace app\views;

use app\controllers\ControllerPanier;

class VueCommande
{

    /**
     * méthode render affiche le type de page en fonction de la méthode
     * 0->confirmation de la commande
     * 1->commande validé
     * @param $methode
     */
    function render($methode) {
        include "header.php";
        switch ($methode) {
            case 0 :
                ?>
                <div>
                    <h1>Confirmez votre commande</h1>
                    <div>
                        <?php
                        $c = new ControllerPanier();
                        $totalCommande = $c->calculTotalPanier();
                        print "<h2>Le prix de la commande est de <b>$totalCommande</b> €</h2>";

                        $app = \Slim\Slim::getInstance();
                        $urlconfirmation = $app->urlFor('passageCommande');
                        $urlPanier = $app->urlFor('panier');
                        $urlProduits = $app->urlFor('listeProduit');

                        print "<div>
                            <a class='btn btn-success' href='$urlconfirmation'>Confirme la commande</a>
                            <a class='btn btn-primary' href='$urlProduits'>Ajouter un produit</a>
                            <a class='btn btn-primary' href='$urlPanier'>Modifier le panier</a>
                        </div>"
                        ?>
                    </div>

                </div>
                <?php
                break;
            case 1:
                ?>
                <div>
                    <h1>Votre commande a bien été prise en compte</h1>
                    <p>Vous serez servis selon les méthodes de livraison des producteurs</p>
                    <?php
                    $app = \Slim\Slim::getInstance();
                    $urlAccueil = $app->urlFor('accueil');

                    print "<div>
                            <a class='btn btn-primary' href='$urlAccueil'>Accueil</a>
                        </div>"
                    ?>
                </div>
<?php
        }
        include "footer.php";
    }
}