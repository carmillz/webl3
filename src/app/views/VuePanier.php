<?php

namespace app\views;

use app\controllers\ControllerPanier;
use app\models\Produit;

class VuePanier
{
    public $app;

    /**
     * méthode render affiche le type de page en fonction de la méthode
     * 0->affiche le panier
     * 1->affiche quand panier vide
     * @param $methode
     */
    function render($methode) {
        include "header.php";

        $this->app = \Slim\Slim::getInstance();
        switch ($methode) {
            case 0 :
                $script = dirname($_SERVER['SCRIPT_NAME'])."/src/app/js/scriptPanier.js";
                echo "<script type=\"text/javascript\" src=\"$script\"></script>"; //ajout du script jquery pour le panier

                ?>
                <div class="col-md-9">
                    <h1>Votre panier</h1>
                    <?php
                        $panier = $_SESSION['panier'];
                        foreach ($panier as $idProd=>$prod){
                            $this->divProduit($idProd, $prod['qty']);
                        }
                    ?>
                </div>

                <div class="col-md-3">
                    <?php
                    $urlVidePanier = $this->app->urlFor('videPanier');
                    $urlPasseCommande = $this->app->urlFor('commande');
                    $c = new ControllerPanier();
                    $prixTotal = $c->calculTotalPanier();
                    print "<h2>Prix total : <br><b><span id='totalPanier'>$prixTotal</span> €</b></h2>
                    <a class=\"btn btn-success\" href=\"$urlPasseCommande\">Passer commande</a>
                    <a class=\"btn btn-primary\" href=\"$urlVidePanier\" id=\"videPanier\">Vider le panier</a>";
                    ?>
                </div>

                <?php break;
            case 1 :
                ?>
                <div class="col-md-12">
                    <h1>Panier</h1>
                    <div class="col-md-12">
                        <h2>Votre panier est vide</h2>
                    </div>
                </div>
                <?php
                break;
        }
        include "footer.php";
    }

    /**
     * div d'un produit dans la liste panier
     * @param $idProd
     * @param $qty
     */
    function divProduit($idProd, $qty) {
        $p = Produit::where('idProduit', '=', $idProd)->first();
        $urlProduit = $this->app->urlFor('produit', array('id'=>$idProd));
        print "<div class='divProduit col-md-12' idProd='$p->idProduit'>
            <div class='col-md-4'>
                <img class='imgProduit' src=\"image/produit/$p->image\">
            </div>
            <div class='col-md-8'>
                <div class='col-md-12'>
                    <h2> $p->nomProduit </h2>
                </div>
                <div class='col-md-8'>
                    <p> $p->description </p>
                    <a class='btn btn-info' href=\"$urlProduit\">Détails</a>
                    <a class='btn btn-primary modifierPanier'>Modifier</a>
                    <a class='btn btn-danger supprimerPanier'>Supprimer</a>
                </div>
                <div class='col-md-4'>
                    <p class='prixu' prix='$p->prix'>Prix: $p->prix €/$p->unité</p>
                    <p class='qty'>Quantité: $qty $p->unité</p>
                    <p class='prixt'><b>Prix total: ".$p->prix*$qty." €</b></p>
                </div>
            </div>
        </div>";
    }
}