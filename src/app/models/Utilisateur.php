<?php
namespace app\models;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Utilisateur pour la table utilisateur
 * @package app\models
 */
class Utilisateur extends Model {
	protected $primaryKey = 'idUtilisateur';
	protected $table = 'utilisateur';
	public $timestamps = false;

	function producteur() { //récupère l'objet producteur (ses infos) pour l'utilisateur
	    return $this->belongsTo('app\models\Producteur', 'idUtilisateur', 'idUtilisateur');
    }
}


?>