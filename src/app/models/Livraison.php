<?php

namespace app\models;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Livraison pour la table livraison
 * @package app\models
 */
class Livraison extends Model
{
    protected $primaryKey = 'idLivraison';
    protected $table = 'livraison';
    public $timestamps = false;

    function produit() { //récupère l'objet du produit a partir des ids
        return $this->hasOne('app\models\Produit', 'idProduit', 'idProduit');
    }

    function client() { //récupère l'objet du client a partir des ids
        return $this->hasOne('app\models\Utilisateur', 'idUtilisateur', 'idReceveur');
    }
}