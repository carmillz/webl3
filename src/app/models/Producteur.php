<?php
namespace app\models;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Producteur pour la table producteur
 * @package app\models
 */
class Producteur extends Model {
	protected $primaryKey = 'idProducteur';
	protected $table = 'producteur';
	public $timestamps = false;
	
	function produits() { //récupère les objets des produits du producteur
        return $this->hasMany('app\models\Produit', 'idProducteur', 'idProducteur');
    }
	
	
}

?>