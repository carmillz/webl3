<?php
namespace app\models;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Produit pour la table produit
 * @package app\models
 */
class Produit extends Model {
	protected $primaryKey = 'idProduit';
	protected $table = 'produit';
	public $timestamps = false;

	function producteur() { //récupère l'objet du producteur a partir des ids
	    return $this->hasOne('app\models\Utilisateur', 'idUtilisateur', 'idProducteur');
    }
}

?>