<?php

namespace app\controllers;


use app\views\VuePanier;
use app\models\Produit;

class ControllerPanier
{
    /**
     * affiche le panier
     */
    function affichePanier() {
        $v = new VuePanier();
        if(empty($_SESSION['panier'])) {
            $v->render(1);
        }else {
            $v->render(0);
        }
    }

    /**
     * ajoute un produit au panier
     */
    function ajoutePanier() {
        $id = $_POST['id'];
        $qty = $_POST['qty'];

        if(isset($_SESSION['panier'][$id])) {
            $_SESSION['panier'][$id]['qty'] += $qty;
        }else {
            $_SESSION['panier'][$id]['qty'] = $qty;
        }
    }

    /**
     * modifie la quantité d'un produit du panier
     */
    function modifiePanier() {
        $id = $_POST['id'];
        $qty = $_POST['qty'];

        if(isset($_SESSION['panier'][$id])) {
            $_SESSION['panier'][$id]['qty'] = $qty;

            $v = new VuePanier();
            $v->app = \Slim\Slim::getInstance();
            $v->divProduit($id, $qty);
        }
    }

    /**
     * supprime un produit du panier
     */
    function supprimePanier() {
        $id = $_POST['id'];

        unset($_SESSION['panier'][$id]);
    }

    /**
     * vide le panier et redirige vers le panier
     */
    function videPanier() {
        $_SESSION['panier'] = array();
    }

    /**
     * affiche le total du panier
     */
    function totalPanier() {
        print $this->calculTotalPanier();
    }

    /**
     * calcul le total du panier
     * @return float
     */
    function calculTotalPanier() {
        $panier = $_SESSION['panier'];
        $totalPanier = 0;
        foreach ($panier as $idProd=>$prod){
            $p = Produit::where('idProduit', '=', $idProd)->first();
            $totalPanier += $p->prix*$prod['qty'];
        }
        return $totalPanier;
    }
}