<?php

namespace app\controllers;

use app\models\Livraison;
use app\models\Produit;
use app\models\Utilisateur;
use app\views\VueCommande;

class ControllerCommande
{

    /**
     * vérifie que l'utilisateur est bien connecter et le renvoie vers la page de connexion sinon
     */
    function verfieCommande() {
        $c = new ControllerUtilisateur();
        if($c->estConnecter()) {
            $this->afficheCommande();
        } else {
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('connexion'));
        }
    }

    /**
     * affiche le récapitulatif des commandes
     */
    function afficheCommande() {
        $v = new VueCommande();
        $v->render(0);
    }

    /**
     * enregistre la commande pour chaque produit du panier
     */
    function passageCommande() {
        $util = Utilisateur::where('pseudo', '=', $_SESSION['pseudo'])->first();
        $idUtilisateur = $util->idUtilisateur;
        foreach ($_SESSION['panier'] as $idProd=>$p) {
            $prod = Produit::where('idProduit', '=', $idProd)->first();

            $livraison = new Livraison();
            $livraison->idLivreur = $prod->idProducteur;
            $livraison->idReceveur = $idUtilisateur;
            $livraison->idProduit = $prod->idProduit;
            $livraison->quantité = $p['qty'];
            $livraison->effectue = false;
            $dt = new \DateTime;
            $livraison->dateCommande = $dt->format('Y-m-d');

            $livraison->save();
        }

        $c = new ControllerPanier();
        $c->videPanier();

        $v = new VueCommande();
        $v->render(1);

    }
}