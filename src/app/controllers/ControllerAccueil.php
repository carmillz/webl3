<?php

namespace app\controllers;

use app\views\Accueil;

class ControllerAccueil
{

    /**
     * affiche la page d'accueil
     */
    function accueil() {
        $accueil = new Accueil();
        $accueil->render(0);
    }
}