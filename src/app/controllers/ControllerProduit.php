<?php
/**
 * Created by PhpStorm.
 * User: Arnaud-asus
 * Date: 05/12/2017
 * Time: 15:05
 */

namespace app\controllers;
use app\models\Produit;
use app\models\Producteur;
use app\models\Utilisateur;
use app\views\VueProduit;
use app\controllers\ControllerAccueil;

class ControllerProduit
{

    /**
     * affiche la liste des produits par défauts
     */
    function listeProduit() {
		$vueProduit=new VueProduit();
		$vueProduit->render(0);
    }

    /**
     * liste des produits a partir des paramètres de recherche
     */
    function rechercheProduit() {
        $v = new VueProduit();
        $produits = Produit::query();
        if(isset($_POST['nom'])) {
            $produits = $produits->where('nomProduit', 'like', '%'.$_POST['nom'].'%');
        }
        if(isset($_POST['categ']) and $_POST['categ'] != 'tous') {
            $produits = $produits->where('categorie', '=', $_POST['categ']);
		}
		if(isset($_POST['producteur']) && $_POST['producteur']!="") {
            $produits = $produits->whereHas('Producteur', function ($query) { //cherche parmis les utilisateurs
                $query->whereHas('Producteur', function ($query) { //cherche parmis les producteurs
                    $query->where('nomProducteur', 'like', '%'.$_POST['producteur'].'%'); //verifie le nom du producteur
                });
            });
		}
        $produits = $produits->get();
        $v->affichageProduits($produits);
    }

    /**
     * affiche la page de détail d'un produit
     * @param $id
     */
    function afficheProduit($id) {
        $v = new VueProduit();
        $produit = Produit::where('idProduit', '=', $id)->first();
        $v->render(1, $produit);
    }

    /**
     * affiche le formulaire d'ajout d'un produit
     */
    function vueAjoutProduit() {
        $v = new VueProduit();
        $v->render(2);
    }

    /**
     * affiche page lorsque le produit est bien ajouter
     */
    function afficheAjoutReussi() {
        $v = new VueProduit();
        $v->render(3);
    }

    /**
     * enregistrement du produit dans la base
     */
    function ajoutProduit()
    {
        $util = Utilisateur::where('pseudo', '=', $_SESSION['pseudo'])->first();
		$prod = Producteur::where('idUtilisateur', '=', $util->idUtilisateur);
        $produit = new Produit();
        $produit->nomProduit = $_POST['nomProduit'];
        $produit->categorie = $_POST['categorie'];
        $produit->prix = $_POST['prix'];
        $produit->description = $_POST['description'];
        $produit->unité = $_POST['unite'];
        $produit->idProducteur = $util->idUtilisateur;

        //upload de l'image
        $ext = substr(strrchr($_FILES['image']['name'],'.'),1);
        $nomImage = strtr($_POST['nomProduit'],'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ','aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
        $nomImage = $nomImage.'.'.$ext;
        $destination = 'image/produit/'.$nomImage;
        $produit->image = $nomImage;

        $produit->save(); //sauvegarde

        $extensions_valides = array('jpg', 'jpeg', 'gif', 'png'); //vérification des extensions de fichier
        $upload = $this->upload('image', $destination, FALSE, $extensions_valides);
        if($upload) {
            var_dump('upload de l\'image réussie');
			$this->afficheAjoutReussi();
        } else {
            var_dump('fail');
        }
    }

    /**
     * fonction pour enregistrement de l'image
     * @param $index
     * @param $destination
     * @param bool $maxsize
     * @param bool $extensions
     * @return bool
     */
    function upload($index,$destination,$maxsize=FALSE,$extensions=FALSE)
    {
        //Test1: fichier correctement uploadé
        if (!isset($_FILES[$index]) OR $_FILES[$index]['error'] > 0) return FALSE;
        //Test2: taille limite
        if ($maxsize !== FALSE AND $_FILES[$index]['size'] > $maxsize) return FALSE;
        //Test3: extension
        $ext = substr(strrchr($_FILES[$index]['name'],'.'),1);
        if ($extensions !== FALSE AND !in_array($ext,$extensions)) return FALSE;
        //Déplacement
        return move_uploaded_file($_FILES[$index]['tmp_name'],$destination);
    }
}