<?php
namespace app\controllers;
use app\models\Utilisateur;
use app\models\Producteur;
use app\views\VueUtilisateur;

class ControllerUtilisateur
{

    /**
     * liste des fournisseurs
     */
	function listeUtilisateur() {
		$vueUtilisateur=new VueUtilisateur();
		$vueUtilisateur->render(4);
	}

    /**
     * page de détail d'un utilisateur
     * @param $id
     */
	function afficheUtilisateur($id) {
        $v = new VueUtilisateur();
        $utilisateur = Utilisateur::where('idUtilisateur', '=', $id)->first();
        $v->render(5, $utilisateur);
    }

    /**
     * formulaire inscription d'un utilisateur
     */
	function vueAjoutUtilisateur() {
        $v = new VueUtilisateur();
        $v->render(0);
    }

    /**
     * affiche validation d'inscription
     */
	function vueUtilisateurAjoute() {
        $v = new VueUtilisateur();
        $v->render(1);
    }

    /**
     * affiche formulaire de conexion
     */
	function vueConnexion() {
        $v = new VueUtilisateur();
        $v->render(2);
    }

    /**
     * indique si l'utilisateur est connecter
     * @return bool
     */
    function estConnecter() {
	    return isset($_SESSION['pseudo']);
    }

    /**
     * récupère son id utilisateur
     * @return mixed
     */
    function getIdUtilisateur() {
	    $util = Utilisateur::where('pseudo', '=', $_SESSION['pseudo'])->first();
	    return $util->idUtilisateur;
    }

    /**
     * vérifie si l'utilisateur est un fournisseur
     */
    function estFournisseur() {
        if(isset($_SESSION['fournisseur']))
            return $_SESSION['fournisseur'] == 1;
        else return false;
    }

    /**
     * verifie si accès possible pour fournisseur
     */
    function verifieAccesFournisseur() {
        if($this->estFournisseur()) {
            return true;
        }else {
            $app = \Slim\Slim::getInstance();
            $app->redirect($app->urlFor('accesRefuse'));
        }
    }

    /**
     * enregistre l'utilisateur dans la base
     */
	public function ajoutUtilisateur(){
		$type = $_POST["type"];
		$nom = $_POST["nom"];
		$prenom = $_POST["prenom"];
		$pseudo = $_POST["pseudo"];
		$mdp = password_hash($_POST["mdp"], PASSWORD_DEFAULT);
		
		if (utilisateur::where('pseudo', '=', $pseudo)->first()==""){ //si pseudo libre
			if ($type=="client"){ //type client
				$utilisateur = array(
					"nom" => $nom,
					"prenom" => $prenom,
					"pseudo" => $pseudo,
					"mdp" => $mdp,
					"fournisseur" => false
				);
				$result = utilisateur::insert($utilisateur);
				$this->vueUtilisateurAjoute();
			}else { //type fournisseur
				$utilisateur = array(
					"nom" => $nom,
					"prenom" => $prenom,
					"pseudo" => $pseudo,
					"mdp" => $mdp,
					"fournisseur" => true
				);
				$result = utilisateur::insert($utilisateur);
				$fourn = utilisateur::where('pseudo', '=', $pseudo)->first();
				$producteur = array(
					"nomProducteur" => $_POST["nomEnt"],
					"adresseProducteur" => $_POST["adresse"],
					"idUtilisateur" => $fourn->idUtilisateur,
					"detailsLivraison" => $_POST["detailLiv"]
				);
				
				$result = producteur::insert($producteur);
				$this->vueUtilisateurAjoute();
			}
			
		}else {
			if (utilisateur::where('mdp','=',$mdp)->first()!=""){
				echo "pasOK";
			}
			echo "Ce nom d'utilisateur n'est pas diponible";
			$this->vueAjoutUtilisateur();
		}
	}

    /**
     * vérifie infos de connexion {pseudo, mdp}
     */
	public function connexionUtilisateur(){
		
		$pseudo = $_POST["pseudo"];
		$mdp = $_POST["mdp"];
		$util = utilisateur::where('pseudo', '=', $pseudo)->first();
		if ($util==""){
			//cet utilisateur n'existe pas
			echo "Cet utilisateur n'existe pas. Réessayez !";
			$this->vueConnexion();
		}else {
			if (password_verify($mdp, $util->mdp)){
				//L'utilisateur existe et a entré le bon mot de passe
				$_SESSION['pseudo'] = $pseudo;
				if ($util->fournisseur==1){
					$_SESSION['fournisseur'] = true;
				}else {
					$_SESSION['fournisseur'] = false;
				}
                $app = \Slim\Slim::getInstance();
                $app->redirect($app->urlFor('accueil'));
			}else {
				echo "Mot de passe erroné. Réessayez !";
				$this->vueConnexion();
			}
		}
	}

    /**
     * déconnecte l'utilisateur
     */
	public function deconnexionUtilisateur(){
		session_destroy();
		echo "Vous êtes maintenant déconnecté";
		$app = \Slim\Slim::getInstance();
		$app->redirect($app->urlFor('accueil'));
	}

    /**
     * affiche page vérification de déconnexion
     */
	public function vueDeconnexion(){
		$v = new VueUtilisateur();
        $v->render(3);
	}
}	
?>