<?php

namespace app\controllers;


use app\models\Livraison;
use app\views\VueLivraison;

class ControllerLivraison
{

    /**
     * affiche la page de la liste des livraisons
     */
    function afficheLivraison() {
        $v = new VueLivraison();
        $v->render(0);
    }

    /**
     * affiche la liste des livraisons lors du changement des options
     */
    function rechercheLivraison() {
        $v = new VueLivraison();
        $c = new ControllerUtilisateur();
        $idUtil = $c->getIdUtilisateur();
        $livraisons = Livraison::query();

        if($_POST['effectue'] == 'false') {
            $livraisons = Livraison::where('idLivreur', '=', $idUtil)
                                    ->where('effectue', '=', 0);
        } else if($_POST['effectue'] == 'true') {
            $livraisons = Livraison::where('idLivreur', '=', $idUtil);
        }
        $livraisons = $livraisons->get();
        $v->affichageLivraisons($livraisons);
    }

    /**
     * rend la livraison effectué
     */
    function livraisonEff() {
        $id = $_POST['idLivraison'];
        $livraison = Livraison::where('idLivraison', '=', $id)->first();
        $livraison->effectue = 1;
        $livraison->save();

        $v = new VueLivraison();
        $v->app = \Slim\Slim::getInstance();

        $v->divLivraison($livraison);
    }
}