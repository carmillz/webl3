<?php

namespace app\controllers;

use app\views\VueErreur;


class ControllerErreur
{

    /**
     * affiche la page du refus d'accès
     */
    function afficheAccesRefuse() {
        $v = new VueErreur();
        $v->render(0);
    }
}