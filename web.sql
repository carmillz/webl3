-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 05 jan. 2018 à 19:22
-- Version du serveur :  5.7.19
-- Version de PHP :  5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `web`
--

-- --------------------------------------------------------

--
-- Structure de la table `livraison`
--

DROP TABLE IF EXISTS `livraison`;
CREATE TABLE IF NOT EXISTS `livraison` (
  `idLivraison` int(11) NOT NULL AUTO_INCREMENT,
  `idLivreur` int(11) NOT NULL,
  `idReceveur` int(11) NOT NULL,
  `idProduit` int(11) NOT NULL,
  `quantité` int(11) NOT NULL,
  `effectue` tinyint(1) NOT NULL,
  `dateCommande` date DEFAULT NULL,
  PRIMARY KEY (`idLivraison`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `livraison`
--

INSERT INTO `livraison` (`idLivraison`, `idLivreur`, `idReceveur`, `idProduit`, `quantité`, `effectue`, `dateCommande`) VALUES
  (15, 25, 19, 6, 1, 1, '2018-01-05'),
  (16, 25, 19, 2, 32, 1, '2018-01-05'),
  (17, 17, 19, 8, 54, 0, '2018-01-05'),
  (18, 17, 19, 9, 45, 0, '2018-01-05'),
  (19, 17, 19, 8, 96, 0, '2018-01-05'),
  (20, 24, 19, 3, 1, 1, '2018-01-05'),
  (21, 25, 19, 6, 2, 0, '2018-01-05');

-- --------------------------------------------------------

--
-- Structure de la table `producteur`
--

DROP TABLE IF EXISTS `producteur`;
CREATE TABLE IF NOT EXISTS `producteur` (
  `idProducteur` int(11) NOT NULL AUTO_INCREMENT,
  `nomProducteur` varchar(20) NOT NULL,
  `adresseProducteur` varchar(40) NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  `detailsLivraison` varchar(200) NOT NULL,
  PRIMARY KEY (`idProducteur`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `producteur`
--

INSERT INTO `producteur` (`idProducteur`, `nomProducteur`, `adresseProducteur`, `idUtilisateur`, `detailsLivraison`) VALUES
  (1, 'Ferme bio', '1 rue de la ferme, 00000 Campagne, Fr', 17, 'Livraison à domicile'),
  (2, 'Parisot', '56, rue de Clairesse, 70200, Lure FR', 24, 'Contacter l\'entreprise pour convenir d\'une éventuelle livraison'),
  (3, 'NetExpress', '30, rue Jeanne d\'arc, 75000, Paris, FR', 25, 'Les services sont assurés au domicile du client'),
  (4, 'Bois cerda', '1, allée des Erables 88200 Epinal FR', 26, 'La livraison n\'est pas assurée par l\'entreprise'),
  (5, 'Mon beau jardin', '99, rue des Roses, 67000 Strasbourg, FR', 10, 'Contacter l\'entreprise pour prendre rendez-vous\r\n');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `idProduit` int(11) NOT NULL AUTO_INCREMENT,
  `nomProduit` varchar(50) NOT NULL,
  `categorie` varchar(20) NOT NULL,
  `prix` double NOT NULL,
  `unité` varchar(50) NOT NULL,
  `idProducteur` int(11) NOT NULL,
  `description` varchar(200) NOT NULL,
  `image` varchar(30) NOT NULL,
  PRIMARY KEY (`idProduit`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`idProduit`, `nomProduit`, `categorie`, `prix`, `unité`, `idProducteur`, `description`, `image`) VALUES
  (6, 'Fauteuil artisanal', 'materiel', 250, 'pièce', 25, 'Prix TTC. Chaise artisanale conçue dans un atelier familial en bois de chêne.', 'fauteuilArtisanal.jpg'),
  (1, 'Tomates bio', 'alimentaire', 7, 'kg', 17, 'Tomates vendus au kilogramme issus de l\'agriculture biologique', 'tomate.jpg'),
  (2, 'Bois de chauffage', 'materiel', 80, 'stère', 25, 'Bois de sapin vendu au stère issu des forêts de Lorraine', 'boisDeChauffage.jpg'),
  (3, 'Depannage informatique', 'service', 88, 'déplacement', 24, 'Dépannage à domicile, déplacement inclus et première heure indivisible.', 'depanageInformatique.jpg'),
  (4, 'Brocolis bio', 'alimentaire', 8, 'kg', 17, 'Brocolis vendus au kilogramme issus de l\'agriculture biologique', 'brocolisBio.jpg'),
  (7, 'Creation de jardin', 'service', 26, 'm²', 24, 'Création d\'un jardin à domicile, prix proposé au mètre carré. Déplacement et matériel inclu.', 'creationDeJardin.jpg'),
  (8, 'Oeufs bio', 'alimentaire', 4.5, 'u', 17, 'Oeufs vendus par douze issus d\'un élevage en plein air', 'oeufBio.jpg'),
  (9, 'lait', 'alimentaire', 1, 'l', 17, 'lait de vache', 'lait.jpg'),
  (10, 'avion', 'materiel', 25000, 'pièce', 17, 'avion rapide', 'avion.jpg'),
  (11, 'Chêne', 'materiel', 50, 'stère', 25, 'Prix d\'un stère de chêne', 'Chêne.jpg'),
  (14, 'coque de téléphone', 'custom', 20, 'pièce', 24, 'coque de téléphone simple', 'coque de tYolYophone.png');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(11) NOT NULL,
  `prenom` varchar(11) NOT NULL,
  `pseudo` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `date_inscription` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fournisseur` tinyint(1) NOT NULL,
  PRIMARY KEY (`idUtilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `nom`, `prenom`, `pseudo`, `mdp`, `date_inscription`, `fournisseur`) VALUES
  (10, 'Pervenche', 'Violette', 'viPervenche', '$2y$10$/qVnlts11Pg7HcVctsgv5Opae55onrVd0mwVLc7iguFU7Rdm7ASei', '2018-01-05 14:53:21', 1),
  (17, 'nom', 'prenom', 'fournisseur', '$2y$10$lGmZAgcVSdtfIj8bcFCLCukLlFZfg1VQL1FsJ5h9fXV2IGpoG4wVS', '2018-01-04 14:35:36', 1),
  (19, 'nom', 'prenom', 'client', '$2y$10$FlhjBVPM55nlgZTkFRfb8.h8ZgaLz2WvrHTUTaqgrDERKKg/pNZHq', '2018-01-04 14:36:30', 0),
  (24, 'Perrin', 'Julie', 'juPerrin', '$2y$10$q3HMyvKHMto.33MVxN5mQOG3Kiro7Z83qmwiTHjNNAv3xqcTR1.Ha', '2018-01-04 17:15:52', 1),
  (25, 'Dupond', 'Jean', 'test1', '$2y$10$3indUH5TCSrwkpW8CpCCu.W6dln53T6PCwaJmIkDVphzLq4nkJt1.', '2018-01-04 17:17:32', 1),
  (26, 'Dubois', 'Olivier', 'bois', '$2y$10$Nwi5MJurv6jMtfGT.Iqb9ePS4BI2dKlWcuwiR5moS.WSRHbkjlsDi', '2018-01-05 13:43:32', 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
